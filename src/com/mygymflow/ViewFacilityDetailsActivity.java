package com.mygymflow;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import com.gymflow.R;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ViewFacilityDetailsActivity extends Activity {
	ArrayList<String> resources = new ArrayList<String>();
	static FacilityDetailsArrayAdapter listOfFacilityHourDetails;
	public static int position = 0;
	protected static String hours_name = null;
	protected static HashMap<String, HashMap<String, ArrayList<String>>> facilityDetails = 
			new HashMap<String, HashMap<String,ArrayList<String>>>();
	protected static HashMap<String,String> facilityNames = new HashMap<String,String>();
	static SparseArray <ArrayList<String>> facilityHours = new SparseArray<ArrayList<String>>();
	
	String startTime[] = null;
	String endTime[] = null;
	Calendar start = Calendar.getInstance();
	Calendar end = Calendar.getInstance();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.facility_details_layout);
		
		//get the facility name
		String facilityName = facilityNames.get(Integer.valueOf(position+1).toString());
		
		//get the facility hours
		HashMap<String, ArrayList<String>> facilityDays = facilityDetails.get(facilityName);
		ArrayList<String> facilityHours = null;
		ViewFacilityDetailsActivity.facilityHours.clear();
		ArrayList<String> list = new ArrayList<String>();
		String hours = null;
		int i=0;
		for(String dayOfWeek:GymFlowActivity.daysOfWeek){
			if(facilityDays.containsKey(dayOfWeek)){
				list=new ArrayList<String>();
				if(dayOfWeek.equalsIgnoreCase("mon")){
					list.add("Monday");
				} else if(dayOfWeek.equalsIgnoreCase("tue")){
					list.add("Tuesday");
				} else if(dayOfWeek.equalsIgnoreCase("wed")){
					list.add("Wednesday");
				} else if(dayOfWeek.equalsIgnoreCase("thu")){
					list.add("Thursday");
				} else if(dayOfWeek.equalsIgnoreCase("fri")){
					list.add("Friday");
				} else if(dayOfWeek.equalsIgnoreCase("sat")){
					list.add("Saturday");
				} else if(dayOfWeek.equalsIgnoreCase("sun")){
					list.add("Sunday");
				}
				facilityHours = facilityDays.get(dayOfWeek);
				hours = loadFacilityHours(facilityHours);
				list.add(hours);
				ViewFacilityDetailsActivity.facilityHours.put(i++, list);
			}
		}
		
		ImageView facilityImg = (ImageView) findViewById(R.id.facilityImg);
		facilityImg.setImageResource(Integer.valueOf((String)OpenGymActivity.facilityDetails.get(facilityName).get(0)));
		
		TextView gymTitle = (TextView) findViewById(R.id.gymTitle);
		Typeface face = Typeface
				.createFromAsset(getAssets(), "fonts/AVERIAB.TTF");
		
		TextView facilityHoursTV = (TextView) findViewById(R.id.facilityHours);
		facilityHoursTV.setTypeface(face);
		facilityHoursTV.setText("Facility Hours");
		
		gymTitle.setTypeface(face);

		gymTitle.setText(Html.fromHtml("<font color=\"#66ccff\">"+OpenGymActivity.facilityDetails.get(facilityName).get(2)+"</font>"));
		
		TextView gymSem = (TextView) findViewById(R.id.gymSem);
		gymSem.setText(Html.fromHtml("<font color=\"#ffffff\">"+hours_name+"</font>"));
		
		for(i=0;i<ViewFacilityDetailsActivity.facilityHours.size();i++){
			resources.add(Integer.valueOf(i).toString());
		}
		
		listOfFacilityHourDetails = new FacilityDetailsArrayAdapter(this, resources);
		ListView facilityHoursList = (ListView) findViewById(R.id.facilityHoursList);
		facilityHoursList.setAdapter(listOfFacilityHourDetails);
		facilityHoursList.setOnItemClickListener(displayFacilityHourDetails);
	}
	
	private String loadFacilityHours(ArrayList<String> facilityHours2) {
		if(facilityHours2.size()<3){
			startTime = facilityHours2.get(0).split(":");
			endTime = facilityHours2.get(1).split(":");
			start.set(0, 0, 0, Integer.valueOf(startTime[0]),
					Integer.valueOf(startTime[1]));
			end.set(0, 0, 0, Integer.valueOf(endTime[0]),
					Integer.valueOf(endTime[1]));
	
			return (TrafficFlowActivity.format.format(start.getTime())
					+ " - "
					+ TrafficFlowActivity.format.format(end.getTime()));
		} else {
			StringBuilder times = new StringBuilder();
			for(int i=0;i<facilityHours2.size()-2;i+=2){
				startTime = facilityHours2.get(0).split(":");
				endTime = facilityHours2.get(1).split(":");
				start.set(0, 0, 0, Integer.valueOf(startTime[0]),
						Integer.valueOf(startTime[1]));
				end.set(0, 0, 0, Integer.valueOf(endTime[0]),
						Integer.valueOf(endTime[1]));
		
				times.append(TrafficFlowActivity.format.format(start.getTime())
						+ " - "
						+ TrafficFlowActivity.format.format(end.getTime())
						+ " , ");
			}
			startTime = facilityHours2.get(facilityHours2.size()-2).split(":");
			endTime = facilityHours2.get(facilityHours2.size()-1).split(":");
			start.set(0, 0, 0, Integer.valueOf(startTime[0]),
					Integer.valueOf(startTime[1]));
			end.set(0, 0, 0, Integer.valueOf(endTime[0]),
					Integer.valueOf(endTime[1]));
	
			times.append(TrafficFlowActivity.format.format(start.getTime())
					+ " - "
					+ TrafficFlowActivity.format.format(end.getTime()));
			return times.toString();
		}
	}

	private OnItemClickListener displayFacilityHourDetails = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
		}
	};
	
	public void onBackPressed() {
		this.finish();
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	};
	
	public void goBack(View view){
		onBackPressed();
	}
}
