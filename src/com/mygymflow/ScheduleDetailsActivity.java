package com.mygymflow;

import java.util.Calendar;

import com.gymflow.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("DefaultLocale")
public class ScheduleDetailsActivity extends Activity {
	static int position = 0;
	Calendar start = Calendar.getInstance();
	Calendar end = Calendar.getInstance();
	String startTime[] = null;
	String endTime[] = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.schedule_details_layout);

		Typeface face = Typeface
				.createFromAsset(OpenScheduleActivity.gymActivity.getAssets(), "fonts/AVERIAB.TTF");

		TextView classInfo = (TextView) findViewById(R.id.classInfo);
		classInfo.setTypeface(face);
		classInfo.setText("Class Info");
		
		face = Typeface
				.createFromAsset(OpenScheduleActivity.gymActivity.getAssets(), "fonts/AVERIA_REGULAR.TTF");

		TextView aboutClass = (TextView) findViewById(R.id.aboutThisClass);
		aboutClass.setTypeface(face);
		aboutClass.setText("About this class");
		
		ImageView scheduleImg = (ImageView) findViewById(R.id.scheduleImg);
		TextView scheduleTitle = (TextView) findViewById(R.id.scheduleTitle);
		TextView scheduleDetails = (TextView) findViewById(R.id.scheduleDetails);
		
		scheduleDetails.setTypeface(face);
		scheduleTitle.setTypeface(face);

		startTime = OpenScheduleActivity.morningScheduleList.get(position)
				.get(4).split(":");
		endTime = OpenScheduleActivity.morningScheduleList.get(position).get(5)
				.split(":");
		start.set(0, 0, 0, Integer.valueOf(startTime[0]),
				Integer.valueOf(startTime[1]));
		end.set(0, 0, 0, Integer.valueOf(endTime[0]),
				Integer.valueOf(endTime[1]));

		scheduleImg.setImageResource(Integer
				.valueOf(GymFlowActivity.scheduleImages
						.get(OpenScheduleActivity.morningScheduleList
								.get(position).get(6).toLowerCase())));

		scheduleTitle
				.setText(Html.fromHtml("<font color='#66ccff'>"
						+ OpenScheduleActivity.morningScheduleList
								.get(position).get(0)
						+ "</font>"
						// start time + end time
						+ "<br/><small><font color='#ffffff'>"
						+ OpenScheduleActivity.format.format(OpenScheduleActivity.cal.getTime())
						+ "<br/>"
						+ TrafficFlowActivity.format.format(start.getTime())
						+ " - "
						+ TrafficFlowActivity.format.format(end.getTime())
						// location
						+ "<br/>"
						+ OpenScheduleActivity.morningScheduleList
								.get(position).get(1)
						+ "</font></small>"
/*						+ "<br/>"
						+ OpenScheduleActivity.morningScheduleList
								.get(position).get(2)*/));
		scheduleDetails.setText(Html.fromHtml("<font color='white'><br/>"
				+ OpenScheduleActivity.morningScheduleList.get(position).get(7)
				+ "</font>"));
	}

	public void onBackPressed() {
		this.finish();
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	};

	public void goBack(View view) {
		onBackPressed();
	}
}
