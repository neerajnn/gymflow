package com.mygymflow;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.flurry.android.FlurryAgent;
import com.gymflow.R;

public class GymFlowActivity extends Activity {
	static boolean done = false;
	static HashMap<String, Integer> scheduleImages = new HashMap<String, Integer>();
	static ArrayList<String> daysOfWeek = new ArrayList<String>();
	static GymFlowActivity thisActivity = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.gym_flow_layout);
		thisActivity = this;

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				scheduleImages.put("barre", R.drawable.barre);
				scheduleImages.put("bodypump", R.drawable.bodypump);
				scheduleImages.put("cardio", R.drawable.cardio);
				scheduleImages.put("fusion", R.drawable.fusion);
				scheduleImages.put("hiphop", R.drawable.hiphop);
				scheduleImages.put("nike", R.drawable.nike);
				scheduleImages.put("steps", R.drawable.steps);
				scheduleImages.put("tahitian", R.drawable.tahitian);
				scheduleImages.put("turbo", R.drawable.turbo);
				scheduleImages.put("trx", R.drawable.trx);
				scheduleImages.put("yoga", R.drawable.yoga);
				scheduleImages.put("condition", R.drawable.condition);
				scheduleImages.put("cycling", R.drawable.cycling);
				scheduleImages.put("piyo", R.drawable.piyo);
				scheduleImages.put("zumba", R.drawable.zumba);

				daysOfWeek.add("Mon");
				daysOfWeek.add("Tue");
				daysOfWeek.add("Wed");
				daysOfWeek.add("Thu");
				daysOfWeek.add("Fri");
				daysOfWeek.add("Sat");
				daysOfWeek.add("Sun");

				startActivity(new Intent(thisActivity,
						TrafficFlowActivity.class));
			}
		}, 1000);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (done)
			android.os.Process.killProcess(android.os.Process.myPid());
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "T2K64Z5MHHNBRRTZ56VG");
	}
	 
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}

}
