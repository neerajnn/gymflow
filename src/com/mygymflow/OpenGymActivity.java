package com.mygymflow;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.gymflow.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

public class OpenGymActivity extends Activity{
	private enum LoadedFacilities{
		NETWORKERROR, NOTFOUND, SUCCESS;
	}
	
	@SuppressWarnings("rawtypes")
	static HashMap<String, ArrayList> facilityDetails = new HashMap<String, ArrayList>();

	static ArrayList<String> resources = new ArrayList<String>();
	static GymFacilityArrayAdapter listOfFacilities;
	static OpenGymActivity thisActivity = null;
	static boolean called = false;
	
	private LoadedFacilities loadedFacilities = LoadedFacilities.SUCCESS;

	private ListView facilityList = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gym_layout);
		thisActivity=this;

		loadFacilityDetails();
		
		Typeface face = Typeface
				.createFromAsset(getAssets(), "fonts/AVERIAB.TTF");
		
		LinearLayout lyonCenter = (LinearLayout) findViewById(R.id.lyonCenter);
		lyonCenter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent call = new Intent(Intent.ACTION_DIAL);
				call.setData(Uri.parse("tel:213.740.5127"));
				call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
				startActivity(call);
			}
		});
		
		TextView myGym = (TextView) findViewById(R.id.myGym);
		myGym.setTypeface(face);
		myGym.setText("My Gym");
		
		TextView openGym = (TextView)findViewById(R.id.openGym);
		openGym.setTypeface(face);
		openGym.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_gym,
				0, 0);
		
		TextView openTraffic = (TextView)findViewById(R.id.openTraffic);
		openTraffic.setTypeface(face);
		openTraffic.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_traffic,
				0, 0);
		openTraffic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openTraffic();
			}
		});

		
		TextView openSchedule = (TextView)findViewById(R.id.openSchedule);
		openSchedule.setTypeface(face);

		int date = Calendar.getInstance().get(Calendar.DATE);
		switch (date) {
		case 1:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d1, 0, 0);
			break;
		case 2:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d2, 0, 0);
			break;
		case 3:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d3, 0, 0);
			break;
		case 4:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d4, 0, 0);
			break;
		case 5:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d5, 0, 0);
			break;
		case 6:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d6, 0, 0);
			break;
		case 7:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d7, 0, 0);
			break;
		case 8:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d8, 0, 0);
			break;
		case 9:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d9, 0, 0);
			break;
		case 10:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d10, 0, 0);
			break;
		case 11:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d11, 0, 0);
			break;
		case 12:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d12, 0, 0);
			break;
		case 13:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d13, 0, 0);
			break;
		case 14:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d14, 0, 0);
			break;
		case 15:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d15, 0, 0);
			break;
		case 16:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d16, 0, 0);
			break;
		case 17:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d17, 0, 0);
			break;
		case 18:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d18, 0, 0);
			break;
		case 19:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d19, 0, 0);
			break;
		case 20:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d20, 0, 0);
			break;
		case 21:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d21, 0, 0);
			break;
		case 22:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d22, 0, 0);
			break;
		case 23:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d23, 0, 0);
			break;
		case 24:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d24, 0, 0);
			break;
		case 25:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d25, 0, 0);
			break;
		case 26:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d26, 0, 0);
			break;
		case 27:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d27, 0, 0);
			break;
		case 28:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d28, 0, 0);
			break;
		case 29:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d29, 0, 0);
			break;
		case 30:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d30, 0, 0);
			break;
		case 31:
			openSchedule.setCompoundDrawablesWithIntrinsicBounds(0,
					R.drawable.icon_d31, 0, 0);
			break;
		}
		
		openSchedule.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openSchedule();
			}
		});
		
		TextView gymTitle = (TextView) findViewById(R.id.gymTitle);
		TextView gymAddr = (TextView) findViewById(R.id.gymAddr);
		TextView facilityHoursText = (TextView) findViewById(R.id.facilityHoursText);

		gymTitle.setTypeface(face);
		face = Typeface
				.createFromAsset(getAssets(), "fonts/AVERIA_REGULAR.TTF");
		gymAddr.setTypeface(face);
		facilityHoursText.setTypeface(face);

		gymTitle.setText(Html.fromHtml("USC Lyon Center"));
		gymAddr.setText(Html.fromHtml("1026 W. 34th Street," +
				"<br/>Los Angeles, CA 90089-2500<br/>(P) 213.740.5127"));
		
		listOfFacilities = new GymFacilityArrayAdapter(this, resources);
		facilityList = (ListView) findViewById(R.id.scheduleList);
		facilityList.setAdapter(listOfFacilities);
		facilityList.setOnItemClickListener(displayFacilityDetails);

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		//in a way, set the width and height of lyonCenter linearlayout
		lyonCenter.measure(size.x, size.y);
		
		LinearLayout gymTitles = (LinearLayout) findViewById(R.id.gymTitles);
		ImageView lyonCenterImg = (ImageView) findViewById(R.id.lyonCenterImg);
		//get the extra space remaining after the image and the text has been drawn
		int remainingWidth = size.x - gymTitles.getMeasuredWidth() - lyonCenterImg.getMeasuredWidth();
		TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		//move the image to the right
		lp.setMargins(remainingWidth/2-20, 0, 0, 0);
		lyonCenterImg.setLayoutParams(lp);
	}
	
	private OnItemClickListener displayFacilityDetails = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
			ViewFacilityDetailsActivity.position=pos;
			ViewFacilityDetailsActivity.facilityHours.clear();
			startActivity(new Intent(OpenGymActivity.this, ViewFacilityDetailsActivity.class));
			overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		}
	};
	
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
		openTraffic();
	};
	public void openTraffic(){
		this.finish();
		startActivity(new Intent(this,TrafficFlowActivity.class));
	}
	public void openSchedule() {
		this.finish();
		startActivity(new Intent(this,OpenScheduleActivity.class));
		overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
	}
	
	private void loadFacilityDetails(){
		new LoadFacilityDetails().execute("");
	}
	
	private class LoadFacilityDetails extends AsyncTask<String, Void, String> {
		private boolean networkError = false;
		
		private ProgressDialog pdia;
		@Override
        protected void onPreExecute(){ 
           super.onPreExecute();
           try{
                pdia = new ProgressDialog(OpenGymActivity.this);
	                pdia.setMessage("Loading...");
	                pdia.show();
           } catch(Exception e){
        	   
           }
        }
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		protected String doInBackground(String... date)
		{
			try{
				//get all the schedules
				URL url = new URL("http://54.243.28.121/web-service/load.initial.data.php");
	
				StringBuilder buffer = new StringBuilder();
		        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
	
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		            buffer.append(inputLine);
				in.close();
				JSONTokener jsonTokener = new JSONTokener(buffer.toString());
				JSONObject results = new JSONObject(jsonTokener);
				JSONArray array = (JSONArray) results.get("today_hours");
				facilityDetails.clear();
			
				for (int i = 0; i < array.length(); i++) {
					JSONObject facilities = (JSONObject) array.get(i);
					if(!resources.contains(facilities.getString("facility_name"))){
						resources.add(facilities.getString("facility_name"));
					}
	
					// get the parameters for the movie
					ArrayList parameters = facilityDetails.get(facilities.getString("facility_name"));
					if(parameters==null){
						parameters = new ArrayList();
						//0
						switch(Integer.valueOf(facilities.getString("facility_id"))){
							case 1:
								parameters.add(Integer.valueOf(R.drawable.facility1).toString());
								break;
							case 2:
								parameters.add(Integer.valueOf(R.drawable.facility2).toString());
								break;
							case 3:
								parameters.add(Integer.valueOf(R.drawable.facility3).toString());
								break;
							case 4:
								parameters.add(Integer.valueOf(R.drawable.facility4).toString());
								break;
							case 5:
								parameters.add(Integer.valueOf(R.drawable.facility5).toString());
								break;
							case 6:
								parameters.add(Integer.valueOf(R.drawable.facility6).toString());
								break;
						}
						//1
						parameters.add(facilities.getString("facility_id"));
						//2
						parameters.add(facilities.getString("facility_name"));
						//3
						parameters.add(facilities.getString("is_open"));
						ArrayList<String> timings = new ArrayList<String>();
						if(Integer.valueOf(facilities.getString("is_open"))==1){
							timings.add(facilities.getString("t_open"));
							timings.add(facilities.getString("t_close"));
							//4
							parameters.add(timings);
						}
					} else {
						if(Integer.valueOf(facilities.getString("is_open"))==1){
							//get the timings
							ArrayList<String> timings = (ArrayList<String>)parameters.get(4);
							timings.add(facilities.getString("t_open"));
							timings.add(facilities.getString("t_close"));
						}
					}
					
					facilityDetails.put(facilities.getString("facility_name"), parameters);
				}
				
				JSONObject normalHours = (JSONObject)results.get("normal_hours");
				ViewFacilityDetailsActivity.hours_name = normalHours.getString("hours_name");
				array = (JSONArray)normalHours.getJSONArray("hours");
				ViewFacilityDetailsActivity.facilityDetails.clear();
				ViewFacilityDetailsActivity.facilityNames.clear();
				for (int i = 0; i < array.length(); i++) {
					JSONObject facilities = (JSONObject) array.get(i);
					String facilityName = facilities.getString("facility_name");
					ViewFacilityDetailsActivity.facilityNames.put(facilities.getString("facility_id"), facilityName);
					
					//get the map of days for that facility
					HashMap<String, ArrayList<String>> facilityHours = ViewFacilityDetailsActivity.facilityDetails.get(facilityName);
					if(facilityHours==null){
						facilityHours = new HashMap<String, ArrayList<String>>();
					}
					//get the list containing day details
					ArrayList<String> day = facilityHours.get(facilities.getString("week_day"));
					if(day==null)
						day = new ArrayList<String>();
					day.add(facilities.getString("t_open"));
					day.add(facilities.getString("t_close"));
					facilityHours.put(facilities.getString("week_day"), day);
					ViewFacilityDetailsActivity.facilityDetails.put(facilityName, facilityHours);
				}
			}catch(Exception e){
				loadedFacilities=LoadedFacilities.NETWORKERROR;
				networkError=true;
				thisActivity.runOnUiThread(new Runnable() {
					public void run() {
						if (loadedFacilities == LoadedFacilities.NETWORKERROR) {
							setConnectionErrorMsg();
						} else if (loadedFacilities == LoadedFacilities.NOTFOUND) {
							setNoClassMsg();
						}
					}
				});
			}
			return null;
		}

		protected void onPostExecute(String result) {
			if(pdia.isShowing())
				pdia.dismiss();
			if(null!=listOfFacilities){
				listOfFacilities.notifyDataSetChanged();
				if(null==resources || resources.size()<=0 ){
					if(!networkError)
					if (null != facilityList) {
						loadedFacilities=LoadedFacilities.NOTFOUND;
						try{
							thisActivity.runOnUiThread(new Runnable() {
								public void run() {
									if (loadedFacilities == LoadedFacilities.NETWORKERROR) {
										setConnectionErrorMsg();
									} else if (loadedFacilities == LoadedFacilities.NOTFOUND) {
										setNoClassMsg();
									}
								}
							});
						}catch(Exception e){
							e.printStackTrace();
						}
						networkError=false;
					}
				}
			}
		}
	}

	private void setNoClassMsg(){
		try {
			facilityList.setEmptyView(findViewById(R.id.noFacilityDetails));
			((TextView) findViewById(R.id.noFacilityDetails))
					.setText("No facility details found!");
		} catch (Exception e1) {

		}
	}
	private void setConnectionErrorMsg(){
		try {
			facilityList.setEmptyView(findViewById(R.id.noFacilityDetails));
			((TextView) findViewById(R.id.noFacilityDetails))
					.setText("Error connecting to the internet.\n Please check your connection!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}