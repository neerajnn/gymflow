/**
 * 
 */
package com.mygymflow;

import java.util.ArrayList;

import com.gymflow.R;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * @author NNN
 *
 */
public class FacilityDetailsArrayAdapter extends ArrayAdapter<String>{
	private final Context context;
	
	public FacilityDetailsArrayAdapter(Context context, ArrayList<String> resources){
		super(context, R.layout.facility_detail_info,resources);
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.facility_detail_info, parent, false);
		TextView facilityDay = (TextView)rowView.findViewById(R.id.facilityDay);
		TextView facilityHour = (TextView)rowView.findViewById(R.id.facilityHour);
		
		Typeface face = Typeface
				.createFromAsset(OpenGymActivity.thisActivity.getAssets(), "fonts/AVERIA_REGULAR.TTF");

		facilityDay.setTypeface(face);
		facilityHour.setTypeface(face);
		
		facilityDay.setText(Html.fromHtml(ViewFacilityDetailsActivity.facilityHours.get(position).get(0)));
		facilityHour.setText(Html.fromHtml(ViewFacilityDetailsActivity.facilityHours.get(position).get(1)));
		
		return rowView;
	}

}
