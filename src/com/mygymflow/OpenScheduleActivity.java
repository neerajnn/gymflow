package com.mygymflow;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.gymflow.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class OpenScheduleActivity extends Activity {
	
	private enum LoadedClasses{
		NETWORKERROR, NOTFOUND, SUCCESS;
	}

	static SparseArray <ArrayList<String>> morningScheduleList = new SparseArray<ArrayList<String>>();
	ListView morningScheduleListView = null;
	
	protected static byte[] b=new byte[100000];
	
	String startTime[] = null;
	
	ArrayList<String> morningResources = new ArrayList<String>();
	ArrayList<String> afternoonResources = new ArrayList<String>();
	static ScheduleArrayAdapter morningSchedules;
	static Calendar cal = Calendar.getInstance();
	TextView scheduleDate = null;
	static SimpleDateFormat format = new SimpleDateFormat("EEEEE, MMM dd y",Locale.getDefault());
	static OpenScheduleActivity thisActivity;
	public static TrafficFlowActivity gymActivity = null;
	Calendar c = Calendar.getInstance();
	protected static boolean left=false;
	protected boolean flinging = true;
	int duration = 0;
	
	private LoadedClasses loadedClasses = LoadedClasses.SUCCESS;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.schedule_layout);
		thisActivity = this;
		
		Typeface face = Typeface
				.createFromAsset(getAssets(), "fonts/AVERIA_REGULAR.TTF");
		
		TextView classSchedule = (TextView) findViewById(R.id.classSchedule);
		classSchedule.setText("Class Schedule");
		
		scheduleDate = (TextView)findViewById(R.id.scheduleDate);
		scheduleDate.setTypeface(face);
		scheduleDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				left=false;
				setTodaysDate();
			}
		});
		setScheduleDate();
		
		TextView openGym = (TextView)findViewById(R.id.openGym);
		openGym.setTypeface(face);
		openGym.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_gym,
				0, 0);
		openGym.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openGym();
			}
		});

		TextView openTraffic = (TextView)findViewById(R.id.openTraffic);
		openTraffic.setTypeface(face);
		openTraffic.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_traffic,
				0, 0);
		openTraffic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openTraffic();
			}
		});

		
		TextView openSchedule = (TextView)findViewById(R.id.openSchedule);
		openSchedule.setTypeface(face);
		int date = Calendar.getInstance().get(Calendar.DATE);
		switch(date){
		case 1: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d1, 0, 0);
		break;
		case 2: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d2, 0, 0);
		break;
		case 3: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d3, 0, 0);
		break;
		case 4: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d4, 0, 0);
		break;
		case 5: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d5, 0, 0);
		break;
		case 6: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d6, 0, 0);
		break;
		case 7: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d7, 0, 0);
		break;
		case 8: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d8, 0, 0);
		break;
		case 9: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d9, 0, 0);
		break;
		case 10: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d10, 0, 0);
		break;
		case 11: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d11, 0, 0);
		break;
		case 12: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d12, 0, 0);
		break;
		case 13: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d13, 0, 0);
		break;
		case 14: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d14, 0, 0);
		break;
		case 15: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d15, 0, 0);
		break;
		case 16: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d16, 0, 0);
		break;
		case 17: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d17, 0, 0);
		break;
		case 18: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d18, 0, 0);
		break;
		case 19: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d19, 0, 0);
		break;
		case 20: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d20, 0, 0);
		break;
		case 21: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d21, 0, 0);
		break;
		case 22: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d22, 0, 0);
		break;
		case 23: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d23, 0, 0);
		break;
		case 24: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d24, 0, 0);
		break;
		case 25: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d25, 0, 0);
		break;
		case 26: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d26, 0, 0);
		break;
		case 27: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d27, 0, 0);
		break;
		case 28: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d28, 0, 0);
		break;
		case 29: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d29, 0, 0);
		break;
		case 30: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d30, 0, 0);
		break;
		case 31: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d31, 0, 0);
		break;
		}
		c.set(Calendar.HOUR_OF_DAY, 10);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.getTime();
		
		morningSchedules = new ScheduleArrayAdapter(this, morningResources);
		morningScheduleListView = (ListView) findViewById(R.id.morningScheduleList);
		morningScheduleListView.setAdapter(morningSchedules);
		
		final GestureDetector gestureDetector = new GestureDetector(this, new MyGestureDetector());
		View.OnTouchListener gestureListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				try{
					morningScheduleListView.onTouchEvent(event);
					gestureDetector.onTouchEvent(event);
				}catch(Exception e){}
				return true;
			}
		};
		morningScheduleListView.setOnTouchListener(gestureListener);
		
		TextView noMorningSchedules = (TextView) findViewById(R.id.noMorningSchedules);
		noMorningSchedules.setTypeface(face);
		LinearLayout linearLayoutHoldingList = (LinearLayout) findViewById(R.id.linearLayoutHoldingList);
		noMorningSchedules.setOnTouchListener(gestureListener);
		linearLayoutHoldingList.setOnTouchListener(gestureListener);

		face = Typeface
				.createFromAsset(getAssets(), "fonts/AVERIAB.TTF");
		classSchedule.setTypeface(face);
		
		runOnUiThread(new Runnable() {
			public void run() {
				if (loadedClasses == LoadedClasses.NETWORKERROR) {
					setConnectionErrorMsg();
				} else if (loadedClasses == LoadedClasses.NOTFOUND) {
					setNoClassMsg();
				}
			}
		});
	}
	
	private void morningListClick(int pos){
		if(OpenScheduleActivity.morningScheduleList.get(pos)!=null){
			ScheduleDetailsActivity.position=pos;
			startActivity(new Intent(thisActivity, ScheduleDetailsActivity.class));
			overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		}
	}
	class MyGestureDetector extends SimpleOnGestureListener{
		
		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			int pos = morningScheduleListView.pointToPosition((int)e.getX(), (int)e.getY());
			morningListClick(pos);
			return super.onSingleTapUp(e);
		}

        @Override 
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        	duration=0;
        	if (e1.getX() - e2.getX()>120){
            	left=false;
            	incrementDate();
            } else if (e2.getX() - e1.getX()>120){
            	left=true;
            	decrementDate();
            } else {
            	super.onFling(e1, e2, velocityX, velocityY);
            }
            return false; 
        }
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
        		float distanceX, float distanceY) {
        	flinging = false;
        	duration=0;
        	return super.onScroll(e1, e2, distanceX, distanceY);
        }
    } 
	
	private void setTodaysDate(){
		cal = Calendar.getInstance();
		scheduleDate.setText("Today");
		morningScheduleList.clear();
		morningResources.clear();
		loadSchedules(TrafficFlowActivity.todayDate.format(cal.getTime()));
	}
	private void setScheduleDate(){
		if(Calendar.getInstance().get(Calendar.DATE)==cal.get(Calendar.DATE)){
			scheduleDate.setText("Today");
		} else {
			scheduleDate.setText(format.format(cal.getTime()));
		}
		morningScheduleList.clear();
		morningResources.clear();
		loadSchedules(TrafficFlowActivity.todayDate.format(cal.getTime()));
	}
	
	public void decrementDate(View view){
		flinging = false;
		left=true;
    	cal.add(Calendar.DATE, -1);
		setScheduleDate();
	}
	public void incrementDate(View view){
		flinging = false;
		left=false;
    	cal.add(Calendar.DATE, 1);
		setScheduleDate();
	}
	
	public void decrementDate(){
		flinging = true;
		cal.add(Calendar.DATE, -1);
		setScheduleDate();
	}
	public void incrementDate(){
		flinging = true;
		cal.add(Calendar.DATE, 1);
		setScheduleDate();
	}
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
		openTraffic();
	};
	public void openTraffic(){
		this.finish();
		startActivity(new Intent(this,TrafficFlowActivity.class));
	}

	public void openGym(){
		this.finish();
		startActivity(new Intent(this,OpenGymActivity.class));
		overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
	}
	
	private class LoadSchedulesOperation extends AsyncTask<String, Void, String> {
		private boolean networkError = false;
		private ProgressDialog pdia;
		@Override
        protected void onPreExecute(){ 
           super.onPreExecute();
           try{
                pdia = new ProgressDialog(OpenScheduleActivity.this);
	                pdia.setMessage("Loading...");
	                pdia.show();
           } catch(Exception e){
        	   
           }
        }
		protected String doInBackground(String... date)
		{
			try{
			//get all the schedules
			URL url = new URL("http://54.243.28.121/web-service/load.classes.php?date="+date[0]);
			StringBuilder buffer = new StringBuilder();
	        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	            buffer.append(inputLine);
			in.close();			
			JSONTokener jsonTokener = new JSONTokener(buffer.toString());
			JSONObject results = new JSONObject(jsonTokener);
			JSONArray array = (JSONArray) results.get("class_schedules");
			morningScheduleList.clear();
			
			int listPos = 0;
			String startTime[]=null;
			boolean morningSet = false;
			boolean afternoonSet = false;
			boolean eveningSet = false;
			for (int i = 0; i < array.length(); i++) {
				JSONObject schedule = (JSONObject) array.get(i);
				//resources.add(movie.getString("title"));

				// get the parameters for the movie
				ArrayList<String> parameters = new ArrayList<String>();
				parameters.add(schedule.getString("class_name"));
				parameters.add(schedule.getString("room"));
				parameters.add(schedule.getString("instructor"));
				parameters.add(schedule.getString("weekday"));
				parameters.add(schedule.getString("start_time"));
				parameters.add(schedule.getString("end_time"));
				parameters.add(schedule.getString("img_name"));
				parameters.add(schedule.getString("description"));
				
				startTime = schedule.getString("start_time").split(":");
				
					if (Integer.valueOf(startTime[0]) < 12) {
						if (!morningSet) {
							morningResources.add("morning");
							OpenScheduleActivity.morningScheduleList.put(listPos++, null);
							morningSet = true;
						}
					} else if (Integer.valueOf(startTime[0]) >= 12
							&& Integer.valueOf(startTime[0]) < 18) {
						if (!afternoonSet) {
							morningResources.add("afternoon");
							OpenScheduleActivity.morningScheduleList.put(listPos++, null);
							afternoonSet = true;
						}
					} else {
						if (!eveningSet) {
							morningResources.add("evening");
							OpenScheduleActivity.morningScheduleList.put(listPos++, null);
							eveningSet = true;
						}
					}
					morningResources.add(Integer.valueOf(i).toString());
					OpenScheduleActivity.morningScheduleList.put(listPos++, parameters);
				
			}

			}catch(Exception e){
				loadedClasses=LoadedClasses.NETWORKERROR;
				networkError=true;
				thisActivity.runOnUiThread(new Runnable() {
					public void run() {
						if (loadedClasses == LoadedClasses.NETWORKERROR) {
							setConnectionErrorMsg();
						} else if (loadedClasses == LoadedClasses.NOTFOUND) {
							setNoClassMsg();
						}
					}
				});
			}
			return null;
		}

		protected void onPostExecute(String result) {
			if(pdia.isShowing())
				pdia.dismiss();
			if(null!=morningSchedules){
				ScheduleArrayAdapter.duration = duration;
				morningSchedules.notifyDataSetChanged();
				if(null==morningResources || morningResources.size()<=0 ){
					if(!networkError)
					if (null != morningScheduleListView) {
						loadedClasses=LoadedClasses.NOTFOUND;
						try{
							thisActivity.runOnUiThread(new Runnable() {
								public void run() {
									if (loadedClasses == LoadedClasses.NETWORKERROR) {
										setConnectionErrorMsg();
									} else if (loadedClasses == LoadedClasses.NOTFOUND) {
										setNoClassMsg();
									}
								}
							});
						}catch(Exception e){
							e.printStackTrace();
						}
						networkError=false;
					}
				}
			}
		}
	}

	protected void loadSchedules(String date) {
		new LoadSchedulesOperation().execute(date);
	}
	
	private void setNoClassMsg(){
		try {
			morningScheduleListView
					.setEmptyView(findViewById(R.id.noMorningSchedules));
			((TextView) findViewById(R.id.noMorningSchedules))
					.setText("No classes found!");
		} catch (Exception e1) {

		}
	}
	private void setConnectionErrorMsg(){
		try {
			morningScheduleListView
					.setEmptyView(findViewById(R.id.noMorningSchedules));
			((TextView) findViewById(R.id.noMorningSchedules))
					.setText("Error connecting to the internet.\n Please check your connection!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

}
