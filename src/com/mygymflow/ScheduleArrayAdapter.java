/**
 * 
 */
package com.mygymflow;

import java.util.ArrayList;
import java.util.Calendar;

import com.gymflow.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author NNN
 * 
 */
@SuppressLint("DefaultLocale")
public class ScheduleArrayAdapter extends ArrayAdapter<String> {
	public static int duration=OpenScheduleActivity.thisActivity.duration;
	private final Context context;
	Calendar start = Calendar.getInstance();
	Calendar end = Calendar.getInstance();
	String startTime[] = null;
	String endTime[] = null;
	ArrayList<String> resources = null;

	public ScheduleArrayAdapter(Context context, ArrayList<String> resources) {
		super(context, R.layout.schedule_info, resources);
		this.context = context;
		this.resources = resources;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.schedule_info, parent, false);
		ImageView schedulePic = (ImageView) rowView.findViewById(R.id.schedulePic);
		TextView scheduleInfo = (TextView) rowView.findViewById(R.id.scheduleInfo);
		ImageView rightArrow = (ImageView) rowView.findViewById(R.id.rightArrow);
		
		Typeface face = Typeface
				.createFromAsset(OpenScheduleActivity.gymActivity.getAssets(), "fonts/AVERIAB.TTF");
		scheduleInfo.setTypeface(face);
		
		if(position<resources.size() && OpenScheduleActivity.morningScheduleList.size()>position){
			if(resources.get(position).equalsIgnoreCase("morning")){
				schedulePic.setVisibility(View.GONE);
				scheduleInfo.setVisibility(View.GONE);
				rightArrow.setVisibility(View.GONE);
				TextView timeOfDay = (TextView)rowView.findViewById(R.id.timeOfDay);
				timeOfDay.setText("Morning");
				timeOfDay.setTypeface(face);
				timeOfDay.setVisibility(View.VISIBLE);
			} else if(resources.get(position).equalsIgnoreCase("afternoon")){
				schedulePic.setVisibility(View.GONE);
				scheduleInfo.setVisibility(View.GONE);
				rightArrow.setVisibility(View.GONE);
				TextView timeOfDay = (TextView)rowView.findViewById(R.id.timeOfDay);
				timeOfDay.setTypeface(face);
				timeOfDay.setText("Afternoon");
				timeOfDay.setVisibility(View.VISIBLE);
			} else if(resources.get(position).equalsIgnoreCase("evening")){
				schedulePic.setVisibility(View.GONE);
				scheduleInfo.setVisibility(View.GONE);
				rightArrow.setVisibility(View.GONE);
				TextView timeOfDay = (TextView)rowView.findViewById(R.id.timeOfDay);
				timeOfDay.setTypeface(face);
				timeOfDay.setText("Evening");
				timeOfDay.setVisibility(View.VISIBLE);
			} else {
				rowView.setBackgroundResource(R.drawable.gray_background);
				rowView.setPadding(0, 5, 0, 5);
				schedulePic.setImageResource(Integer
						.valueOf(GymFlowActivity.scheduleImages
								.get(OpenScheduleActivity.morningScheduleList
										.get(position).
										get(6).toLowerCase())));
		
				startTime = OpenScheduleActivity.morningScheduleList.get(position)
						.get(4).split(":");
				endTime = OpenScheduleActivity.morningScheduleList.get(position).get(5)
						.split(":");
				start.set(0, 0, 0, Integer.valueOf(startTime[0]),
						Integer.valueOf(startTime[1]));
				end.set(0, 0, 0, Integer.valueOf(endTime[0]),
						Integer.valueOf(endTime[1]));
		
				// class name
				scheduleInfo
						.setText(Html.fromHtml("<font color='black'>"
								+ OpenScheduleActivity.morningScheduleList
										.get(position).get(0)
								+ "</font>"
								// location
								+ "<br/><small><font color='#b1b1b1'>"
								+ OpenScheduleActivity.morningScheduleList
										.get(position).get(1)
								+ "</font>"
								// start time + end time
								+ "<br/><font color='black'>"
								+ TrafficFlowActivity.format.format(start.getTime())
								+ " - "
								+ TrafficFlowActivity.format.format(end.getTime())
								+ "</font></small>"));
				
				rowView.clearAnimation();
				if(OpenScheduleActivity.thisActivity.flinging)
					if(OpenScheduleActivity.left){
						Animation animation = AnimationUtils.loadAnimation(OpenScheduleActivity.thisActivity, R.anim.slide_in_left);
						animation.setDuration(300);
						rowView.startAnimation(animation);
					} else {
						Animation animation = AnimationUtils.loadAnimation(OpenScheduleActivity.thisActivity, R.anim.slide_in_right);
						animation.setDuration(300);
						rowView.startAnimation(animation);
					}
			}
		}
		return rowView;
	}
}
