package com.mygymflow;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.TreeMap;

import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gymflow.R;
import com.mygymflow.chartengine.ChartFactory;
import com.mygymflow.chartengine.GraphicalView;
import com.mygymflow.chartengine.chart.BarChart.Type;
import com.mygymflow.chartengine.chart.XYChart;
import com.mygymflow.chartengine.model.CategorySeries;
import com.mygymflow.chartengine.model.XYMultipleSeriesDataset;
import com.mygymflow.chartengine.renderer.SimpleSeriesRenderer;
import com.mygymflow.chartengine.renderer.XYMultipleSeriesRenderer;

public class TrafficFlowActivity extends Activity{
	
	private enum LoadedTraffic{
		NETWORKERROR, NOTFOUND, SUCCESS;
	}
	
	public static TrafficFlowActivity thisActivity;
	public static Integer point = 0;
	public static Calendar cal = null;
	private Calendar today = null;
	
	public static CategorySeries series = null;
	
	public static int open_index = 0;

	static int x=0;
	static SimpleDateFormat todayDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
	public static SimpleDateFormat format = new SimpleDateFormat("hh:mm a",Locale.getDefault());
	static SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEE, MMM dd y",Locale.getDefault());
	
	int hourOfTheDay = 0;
	
	TextView trafficDate = null;
	LinearLayout layout = null;
	
	public static Calendar calendar = Calendar.getInstance();

	public static TextView timeSelected = null;
	public static TextView crowdStrength = null;
	public static TextView percentFull = null;
	public static TextView gymStatus = null;
	
	public static ImageView bubble = null;

	GraphicalView graphicalView = null;
	
	static HashMap <String, TreeMap<String, String>> TrafficDetails = new HashMap<String, TreeMap<String,String>>();
	static TreeMap<String, String> UpdatedTrafficDetails = new TreeMap<String,String>();
	static boolean currentTraffic = false;
	public static TextView currentTrafficTV = null;
	
	XYMultipleSeriesRenderer renderer = null;

	LoadedTraffic loadedTraffic = LoadedTraffic.SUCCESS;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		thisActivity = this;
		OpenScheduleActivity.gymActivity = this;
		
		Typeface face = Typeface
				.createFromAsset(getAssets(), "fonts/AVERIA_REGULAR.TTF");
		
		currentTrafficTV = (TextView) findViewById(R.id.currentTraffic);
		
		timeSelected = (TextView) findViewById(R.id.timeSelected);
		timeSelected.setTypeface(face);
		
		percentFull = (TextView) findViewById(R.id.percentFull);
		percentFull.setTypeface(face);
		
		crowdStrength = (TextView) findViewById(R.id.crowdStrength);
		crowdStrength.setTypeface(face);
		
		gymStatus = (TextView) findViewById(R.id.gymStatus);
		
		bubble = (ImageView) findViewById(R.id.bubble);
		
		trafficDate = (TextView) findViewById(R.id.trafficDate);
		trafficDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				/*currentTraffic=true;
				loadUpdatedDetails();*/
			}
		});
		
		TextView openGym = (TextView)findViewById(R.id.openGym);
		openGym.setTypeface(face);
		
		openGym.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_gym,
				0, 0);
		openGym.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openGym();
			}
		});

		TextView openTraffic = (TextView)findViewById(R.id.openTraffic);
		openTraffic.setTypeface(face);
		
		openTraffic.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_traffic,
				0, 0);
		openTraffic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			}
		});
		
		TextView openSchedule = (TextView)findViewById(R.id.openSchedule);
		openSchedule.setTypeface(face);
		
		int date = Calendar.getInstance().get(Calendar.DATE);
		switch(date){
		case 1: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d1, 0, 0);
		break;
		case 2: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d2, 0, 0);
		break;
		case 3: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d3, 0, 0);
		break;
		case 4: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d4, 0, 0);
		break;
		case 5: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d5, 0, 0);
		break;
		case 6: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d6, 0, 0);
		break;
		case 7: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d7, 0, 0);
		break;
		case 8: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d8, 0, 0);
		break;
		case 9: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d9, 0, 0);
		break;
		case 10: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d10, 0, 0);
		break;
		case 11: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d11, 0, 0);
		break;
		case 12: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d12, 0, 0);
		break;
		case 13: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d13, 0, 0);
		break;
		case 14: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d14, 0, 0);
		break;
		case 15: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d15, 0, 0);
		break;
		case 16: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d16, 0, 0);
		break;
		case 17: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d17, 0, 0);
		break;
		case 18: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d18, 0, 0);
		break;
		case 19: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d19, 0, 0);
		break;
		case 20: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d20, 0, 0);
		break;
		case 21: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d21, 0, 0);
		break;
		case 22: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d22, 0, 0);
		break;
		case 23: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d23, 0, 0);
		break;
		case 24: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d24, 0, 0);
		break;
		case 25: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d25, 0, 0);
		break;
		case 26: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d26, 0, 0);
		break;
		case 27: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d27, 0, 0);
		break;
		case 28: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d28, 0, 0);
		break;
		case 29: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d29, 0, 0);
		break;
		case 30: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d30, 0, 0);
		break;
		case 31: openSchedule.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_d31, 0, 0);
		break;
		}
		openSchedule.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openSchedule();
			}
		});
		
		layout = (LinearLayout) findViewById(R.id.chart);
		today = Calendar.getInstance();
		today.set(Calendar.MILLISECOND, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.HOUR, 0);
		
		face = Typeface
				.createFromAsset(getAssets(), "fonts/AVERIA_REGULAR.TTF");
		trafficDate.setTypeface(face);
		face = Typeface
				.createFromAsset(getAssets(), "fonts/AVERIAB.TTF");
		gymStatus.setTypeface(face);
		currentTrafficTV.setTypeface(face);
		cal = Calendar.getInstance();
		loadTrafficDetails();
	}
	public void now(View view){
		loadUpdatedDetails();
	}
	private void setTodayDate(){
		cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);
	    trafficDate.setText("Today");
		drawBar();
	}
	private void setTrafficDate(){
		if(Calendar.getInstance().get(Calendar.DATE)==cal.get(Calendar.DATE)){
			trafficDate.setText("Today");
			currentTraffic=true;
			/*loadUpdatedDetails();*/
			setTodayDate();
		} else {
			trafficDate.setText(dateFormat.format(cal.getTime()));
			drawBar();
		}
	}
	
	public void decrementDate(View view){
		currentTraffic=false;
		cal.add(Calendar.DATE, -1);
		
		String date = todayDate.format(cal.getTime());
		TreeMap<String, String> traffic = TrafficDetails.get(date);
		
		if(null==traffic){
			((Button)findViewById(R.id.decrementButton)).setEnabled(false);
	  		  ((Button)findViewById(R.id.decrementButton)).setAlpha(0.6f);
	  		/*showDialog(0);*/
		} else {
		
			if(TrafficDetails.size()<1){
				loadTrafficDetails();
			} else {
				setTrafficDate();
			}
		}
	}
	public void incrementDate(View view){
		currentTraffic=false;
		cal.add(Calendar.DATE, 1);
		String date = todayDate.format(cal.getTime());
		TreeMap<String, String> traffic = TrafficDetails.get(date);
		
		if(null==traffic){
			((Button)findViewById(R.id.incrementButton)).setEnabled(false);
  		  ((Button)findViewById(R.id.incrementButton)).setAlpha(0.6f);
  		  /*showDialog(0);*/
		} else {
			if(TrafficDetails.size()<1){
				loadTrafficDetails();
			} else {
				setTrafficDate();
			}
		}
	}
	@SuppressWarnings("deprecation")
	private void drawBar(){
		XYMultipleSeriesRenderer renderer = getBarDemoRenderer();
	      XYMultipleSeriesDataset dataset = getBarDemoDataset();
	      if(dataset!=null){
			graphicalView = ChartFactory.getBarChartView(this, dataset,renderer, Type.DEFAULT);
			layout.removeAllViews();
			layout.addView(graphicalView);
			setChartSettings(renderer, dataset);
			((Button)findViewById(R.id.decrementButton)).setEnabled(true);
			((Button)findViewById(R.id.incrementButton)).setEnabled(true);
			((Button)findViewById(R.id.decrementButton)).setAlpha(1.0f);
			((Button)findViewById(R.id.incrementButton)).setAlpha(1.0f);
	      } else {
	    	  showDialog(0);
	      }
	}

	private XYMultipleSeriesDataset getBarDemoDataset() {
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		series = new CategorySeries("");
		int minutes = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) * 60
				+ Calendar.getInstance().get(Calendar.MINUTE);
		minutes /= 15;
		String date = todayDate.format(cal.getTime());
		TreeMap<String, String> traffic = TrafficDetails.get(date);
		
		Set<String> keySet = null;
		Object[] keys = null;
		open_index=0;
		point=0;
		 if(currentTraffic){
				if(null!=UpdatedTrafficDetails && UpdatedTrafficDetails.size()>0){
					keySet = UpdatedTrafficDetails.keySet();
					keys = keySet.toArray();
					open_index = Integer.valueOf(UpdatedTrafficDetails.get(keys[1]));
					for(int i=0;i<Integer.valueOf(UpdatedTrafficDetails.get(keys[1]));i++){
						series.add("t"+i, 0);
					}
		
					boolean set = false;
					for (int i = 2; i < keys.length; i++) {
						series.add((String) keys[i], Double.valueOf(UpdatedTrafficDetails.get(keys[i])));
						if (((String) keys[i]).compareTo("t" + minutes) == 0) {
							if (!set) {
								point = i+Integer.valueOf(UpdatedTrafficDetails.get(keys[1]))-1;
								set = true;
							}
						}
					}
					
					if(point<=0){
						point = open_index;
					}
					dataset.addSeries(series.toXYSeries());
					return dataset;
				}
		} else if (null != traffic) {
			keySet = traffic.keySet();
			keys = keySet.toArray();
			if (cal.after(today)) {
				for (int i = 0; i < Integer.valueOf(traffic.get(keys[1])); i++) {
					series.add("t" + i, 0);
				}
				open_index = Integer.valueOf(traffic.get(keys[1]));

				for (int i = 2; i < keys.length; i++) {
					series.add((String) keys[i],
							Double.valueOf(traffic.get(keys[i])));
				}
				point = open_index - 11;
			} else if (cal.before(today)) {
				for (int i = 0; i < Integer.valueOf(traffic.get(keys[1])); i++) {
					series.add("t" + i, 0);
				}
				open_index = Integer.valueOf(traffic.get(keys[1]));
				for (int i = 2; i < keys.length; i++) {
					series.add((String) keys[i],
							Double.valueOf(traffic.get(keys[i])));
				}
				point = keys.length + open_index - 1;
			}
			dataset.addSeries(series.toXYSeries());
			return dataset;
		}
		return null;
	}

	public XYMultipleSeriesRenderer getBarDemoRenderer() {
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		renderer.setAxisTitleTextSize(16);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);

		renderer.setXLabels(0);
		renderer.addXTextLabel(1, "12am");
		renderer.addXTextLabel(5, "");
		renderer.addXTextLabel(9, "");
		renderer.addXTextLabel(13, "");
		renderer.addXTextLabel(17, "");
		renderer.addXTextLabel(21, "");
		renderer.addXTextLabel(25, "6am");
		renderer.addXTextLabel(29, "7am");
		renderer.addXTextLabel(33, "8am");
		renderer.addXTextLabel(37, "9am");
		renderer.addXTextLabel(41, "10am");
		renderer.addXTextLabel(45, "11am");
		renderer.addXTextLabel(49, "12pm");
		renderer.addXTextLabel(53, "1pm");
		renderer.addXTextLabel(57, "2pm");
		renderer.addXTextLabel(61, "3pm");
		renderer.addXTextLabel(65, "4pm");
		renderer.addXTextLabel(69, "5pm");
		renderer.addXTextLabel(73, "6pm");
		renderer.addXTextLabel(77, "7pm");
		renderer.addXTextLabel(81, "8pm");
		renderer.addXTextLabel(85, "9pm");
		renderer.addXTextLabel(89, "10pm");
		renderer.addXTextLabel(93, "11pm");
		renderer.addXTextLabel(97, "12am");

		renderer.setBarSpacing(0.0001);
		renderer.setMargins(new int[] { 50, 30, 1, 0 });
		SimpleSeriesRenderer r = new SimpleSeriesRenderer();

		r.setColor(Color.BLUE);
		renderer.addSeriesRenderer(r);
		return renderer;
	}

	private void setChartSettings(XYMultipleSeriesRenderer renderer,
			XYMultipleSeriesDataset dataset) {
		if(cal.after(today)){
			renderer.setXAxisMin((double)open_index-XYChart.initialNumOfBars+3);
			renderer.setXAxisMax((double)open_index+XYChart.initialNumOfBars);
		} else if(cal.before(today)){
			double[] startStop = dataset.getSeriesAt(0).getMaxRange();
			renderer.setXAxisMin(startStop[0]);
			renderer.setXAxisMax(startStop[1]);
		} else {
			double[] startStop = dataset.getSeriesAt(0).getRange();
			renderer.setXAxisMin(startStop[0]);
			renderer.setXAxisMax(startStop[1]);
			
			this.renderer = renderer;
			
			new AnimateBarChart().execute(new String[] {startStop[0]+"", startStop[1]+""});
		}
		renderer.setPanLimits(new double[]{open_index-12, dataset.getSeriesAt(0).getMaxX()+14, 0, 300});
		renderer.setYAxisMin(0);
		renderer.setYAxisMax(300);
	}
	
	private class AnimateBarChart extends AsyncTask<String, Void, String> {
		protected String doInBackground(String... params)
		{
			double start = Double.valueOf(params[0]);
			double stop = Double.valueOf(params[1]);
			double min = 0, max=23;
			while(min<start && max<stop){
				try {
					Thread.sleep(7);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				renderer.setXAxisMin(min++);
				renderer.setXAxisMax(max++);
				graphicalView.repaint();
			}
			renderer.setXAxisMin(start);
			renderer.setXAxisMax(stop);
			graphicalView.repaint();
			return null;
		}
	}
	
	public void openSchedule() {
		this.finish();
		startActivity(new Intent(this,OpenScheduleActivity.class));
		overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
	}
	public void openGym(){
		this.finish();
		startActivity(new Intent(this,OpenGymActivity.class));
		overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
	}
	@Override
	public void onBackPressed() {
		GymFlowActivity.done=true;
		this.finish();
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialog = null;
		Builder builder1 = null;
		switch(id){
		case 0:builder1 = new AlertDialog.Builder(this);
		builder1.setMessage("No Traffic data found");
		break;
		case 1:builder1 = new AlertDialog.Builder(this);
		builder1.setMessage("Oops! We couldn't reach our servers. Please check your internet connection.");
		break;
		}
			builder1.setCancelable(true);
			builder1.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							
						}
					});
			dialog = builder1.create();
			dialog.show();
		return dialog;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		loadUpdatedDetails();
	}
	
	protected void loadTrafficDetails(){
		new LoadTrafficDetails().execute("");
	}
	
	private class LoadTrafficDetails extends AsyncTask<String, Void, String> {
		private ProgressDialog pdia;
		private boolean networkError = false;
		@Override
        protected void onPreExecute(){ 
			super.onPreExecute();
			pdia = new ProgressDialog(TrafficFlowActivity.this);
			pdia.setMessage("Loading...");
			pdia.show();
        }
		@SuppressWarnings("rawtypes")
		protected String doInBackground(String... date)
		{
			try{
				TrafficDetails.clear();
				URL url = new URL("http://54.243.28.121/web-service/load.initial.data.php");
	
				StringBuilder buffer = new StringBuilder();
		        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
	
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		            buffer.append(inputLine);
				in.close();
				JSONTokener jsonTokener = new JSONTokener(buffer.toString());
				JSONObject results = new JSONObject(jsonTokener);
				JSONObject predictedTraffic = (JSONObject) results.get("predicted_traffic");
				Iterator iterator = predictedTraffic.keys();
				while (iterator.hasNext()) {
					TreeMap<String, String> dayDetails = new TreeMap<String, String>();
					//HashMap<String, TreeMap<String,String>>();
					//get the day
					String day = (String) iterator.next();
					JSONObject abc = (JSONObject)predictedTraffic.get(day);
					Iterator itr = abc.keys();
					while(itr.hasNext()){
						String key = (String) itr.next();
						String value = (String) abc.get(key);
						dayDetails.put(key, value);
					}
					TrafficDetails.put(day, dayDetails);
				}
			}catch(Exception e){
				networkError = true;
				loadedTraffic=LoadedTraffic.NETWORKERROR;
				thisActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (loadedTraffic == LoadedTraffic.NETWORKERROR) {
							showDialog(1);
						} else if (loadedTraffic == LoadedTraffic.NOTFOUND) {
							showDialog(0);
						}
					}
				});
			}
			return null;
		}

		protected void onPostExecute(String result) {
			if(pdia.isShowing())
				pdia.dismiss();
			/*if(!networkError)
				if(!currentTraffic)
					setTrafficDate();*/
		}
	}
	
	
	protected void loadUpdatedDetails(){
		if(TrafficDetails.size()<1){
			loadTrafficDetails();
		}
		new LoadUpdatedDetails().execute("");
	}
	
	private class LoadUpdatedDetails extends AsyncTask<String, Void, String> {
		private ProgressDialog pdia;
		private boolean networkError = false;
		@Override
        protected void onPreExecute(){ 
			super.onPreExecute();
			pdia = new ProgressDialog(TrafficFlowActivity.this);
			pdia.setMessage("Loading...");
			pdia.show();
        }
		@SuppressWarnings("rawtypes")
		protected String doInBackground(String... date)
		{
			try{
				URL url = new URL("http://54.243.28.121/web-service/load.updated.data.php");
	
				StringBuilder buffer = new StringBuilder();
		        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
	
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		            buffer.append(inputLine);
				in.close();
				if(null!=UpdatedTrafficDetails)
					UpdatedTrafficDetails.clear();
				else
					UpdatedTrafficDetails = new TreeMap<String, String>();
				
				JSONTokener jsonTokener = new JSONTokener(buffer.toString());
				JSONObject results = new JSONObject(jsonTokener);
				JSONObject predictedTraffic = (JSONObject) results.get("updated_traffic");
				Iterator iterator = predictedTraffic.keys();
				while (iterator.hasNext()) {
					String key = (String) iterator.next();
					String value = (String) predictedTraffic.get(key);
					UpdatedTrafficDetails.put(key, value);
				}
				
				int minutes = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) * 60
						+ Calendar.getInstance().get(Calendar.MINUTE);
				minutes /= 15;
				
				predictedTraffic = (JSONObject) results.get("updated_prediction");
				iterator = predictedTraffic.keys();
				while (iterator.hasNext()) {
					String key = (String) iterator.next();
					
					if (key.equalsIgnoreCase("open_index") || key.equalsIgnoreCase("close_index") || Integer.valueOf(key.split("t")[1])>minutes) {
						String value = predictedTraffic.get(key)+"";
						UpdatedTrafficDetails.put(key, value);
					}
				}
			}catch(Exception e){
				networkError = true;
				loadedTraffic=LoadedTraffic.NETWORKERROR;
				thisActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (loadedTraffic == LoadedTraffic.NETWORKERROR) {
							showDialog(1);
						} else if (loadedTraffic == LoadedTraffic.NOTFOUND) {
							showDialog(0);
						}
					}
				});
			}
			return null;
		}

		protected void onPostExecute(String result) {
			if(pdia.isShowing())
				pdia.dismiss();
			if(!networkError){
				currentTraffic = true;
				setTodayDate();
			}
		}
	}
}
