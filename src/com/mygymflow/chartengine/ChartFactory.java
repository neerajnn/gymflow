/**
 * Copyright (C) 2009 - 2012 SC 4ViewSoft SRL
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mygymflow.chartengine;

import com.mygymflow.chartengine.chart.BarChart;
import com.mygymflow.chartengine.chart.XYChart;
import com.mygymflow.chartengine.chart.BarChart.Type;
import com.mygymflow.chartengine.model.CategorySeries;
import com.mygymflow.chartengine.model.MultipleCategorySeries;
import com.mygymflow.chartengine.model.XYMultipleSeriesDataset;
import com.mygymflow.chartengine.renderer.DefaultRenderer;
import com.mygymflow.chartengine.renderer.XYMultipleSeriesRenderer;

import android.content.Context;
import android.content.Intent;

/**
 * Utility methods for creating chart views or intents.
 */
public class ChartFactory {
  /** The key for the chart data. */
  public static final String CHART = "chart";

  /** The key for the chart graphical activity title. */
  public static final String TITLE = "title";

  private ChartFactory() {
    // empty
  }

    /**
   * Creates a bar chart view.
   * 
   * @param context the context
   * @param dataset the multiple series dataset (cannot be null)
   * @param renderer the multiple series renderer (cannot be null)
   * @param type the bar chart type
   * @return a bar chart graphical view
   * @throws IllegalArgumentException if dataset is null or renderer is null or
   *           if the dataset and the renderer don't include the same number of
   *           series
   */
  public static final GraphicalView getBarChartView(Context context,
      XYMultipleSeriesDataset dataset, XYMultipleSeriesRenderer renderer, Type type) {
    checkParameters(dataset, renderer);
    XYChart chart = new BarChart(dataset, renderer, type);
    return new GraphicalView(context, chart);
  }

  /**
   * Creates a bar chart intent that can be used to start the graphical view
   * activity.
   * 
   * @param context the context
   * @param dataset the multiple series dataset (cannot be null)
   * @param renderer the multiple series renderer (cannot be null)
   * @param type the bar chart type
   * @return a bar chart intent
   * @throws IllegalArgumentException if dataset is null or renderer is null or
   *           if the dataset and the renderer don't include the same number of
   *           series
   */
  public static final Intent getBarChartIntent(Context context, XYMultipleSeriesDataset dataset,
      XYMultipleSeriesRenderer renderer, Type type) {
    return getBarChartIntent(context, dataset, renderer, type, "");
  }

  /**
   * Creates a bar chart intent that can be used to start the graphical view
   * activity.
   * 
   * @param context the context
   * @param dataset the multiple series dataset (cannot be null)
   * @param renderer the multiple series renderer (cannot be null)
   * @param type the bar chart type
   * @param activityTitle the graphical chart activity title
   * @return a bar chart intent
   * @throws IllegalArgumentException if dataset is null or renderer is null or
   *           if the dataset and the renderer don't include the same number of
   *           series
   */
  public static final Intent getBarChartIntent(Context context, XYMultipleSeriesDataset dataset,
      XYMultipleSeriesRenderer renderer, Type type, String activityTitle) {
    checkParameters(dataset, renderer);
    Intent intent = new Intent(context, GraphicalActivity.class);
    BarChart chart = new BarChart(dataset, renderer, type);
    intent.putExtra(CHART, chart);
    intent.putExtra(TITLE, activityTitle);
    return intent;
  }


  /**
   * Checks the validity of the dataset and renderer parameters.
   * 
   * @param dataset the multiple series dataset (cannot be null)
   * @param renderer the multiple series renderer (cannot be null)
   * @throws IllegalArgumentException if dataset is null or renderer is null or
   *           if the dataset and the renderer don't include the same number of
   *           series
   */
  private static void checkParameters(XYMultipleSeriesDataset dataset,
      XYMultipleSeriesRenderer renderer) {
    if (dataset == null || renderer == null
        || dataset.getSeriesCount() != renderer.getSeriesRendererCount()) {
      throw new IllegalArgumentException(
          "Dataset and renderer should be not null and should have the same number of series");
    }
  }

  /**
   * Checks the validity of the dataset and renderer parameters.
   * 
   * @param dataset the category series dataset (cannot be null)
   * @param renderer the series renderer (cannot be null)
   * @throws IllegalArgumentException if dataset is null or renderer is null or
   *           if the dataset number of items is different than the number of
   *           series renderers
   */
  private static void checkParameters(CategorySeries dataset, DefaultRenderer renderer) {
    if (dataset == null || renderer == null
        || dataset.getItemCount() != renderer.getSeriesRendererCount()) {
      throw new IllegalArgumentException(
          "Dataset and renderer should be not null and the dataset number of items should be equal to the number of series renderers");
    }
  }

  /**
   * Checks the validity of the dataset and renderer parameters.
   * 
   * @param dataset the category series dataset (cannot be null)
   * @param renderer the series renderer (cannot be null)
   * @throws IllegalArgumentException if dataset is null or renderer is null or
   *           if the dataset number of items is different than the number of
   *           series renderers
   */
  private static void checkParameters(MultipleCategorySeries dataset, DefaultRenderer renderer) {
    if (dataset == null || renderer == null
        || !checkMultipleSeriesItems(dataset, renderer.getSeriesRendererCount())) {
      throw new IllegalArgumentException(
          "Titles and values should be not null and the dataset number of items should be equal to the number of series renderers");
    }
  }

  private static boolean checkMultipleSeriesItems(MultipleCategorySeries dataset, int value) {
    int count = dataset.getCategoriesCount();
    boolean equal = true;
    for (int k = 0; k < count && equal; k++) {
      equal = dataset.getValues(k).length == dataset.getTitles(k).length;
    }
    return equal;
  }

}
