/**
 * 
 */
package com.mygymflow;

import java.util.ArrayList;
import java.util.Calendar;

import com.gymflow.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author NNN
 *
 */
public class GymFacilityArrayAdapter extends ArrayAdapter<String>{
	private final Context context;
	private ArrayList<String> resources;

	Calendar start = Calendar.getInstance();
	Calendar end = Calendar.getInstance();
	String startTime[] = null;
	String endTime[] = null;
	
	public GymFacilityArrayAdapter(Context context, ArrayList<String> resources){
		super(context, R.layout.facility_info,resources);
		this.context = context;
		this.resources = resources;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.facility_info, parent, false);
		Typeface face = Typeface
				.createFromAsset(OpenScheduleActivity.gymActivity.getAssets(), "fonts/AVERIAB.TTF");

		if(OpenGymActivity.facilityDetails.size()>0){
			ImageView facilityPic = (ImageView)rowView.findViewById(R.id.facilityPic);
			TextView facilityInfo = (TextView)rowView.findViewById(R.id.facilityInfo);
			TextView openClose = (TextView)rowView.findViewById(R.id.openClose);
			
			facilityInfo.setTypeface(face);
			
			String info = "<font color='black'>"+
					//facility name
					OpenGymActivity.facilityDetails.get(resources.get(position)).get(2)+"</font><br/><small><font color='#4c4c4c'>Today's Hours:<br/>";
			
			if(Integer.valueOf((String)OpenGymActivity.facilityDetails.get(resources.get(position)).get(3))==1){
				@SuppressWarnings("unchecked")
				ArrayList<String> timings = (ArrayList<String>)OpenGymActivity.facilityDetails.get(resources.get(position)).get(4);
				if(timings.size()>2){
					for(int i=0;i<timings.size()-2;i+=2){
						startTime = timings.get(i).split(":");
						endTime = timings.get(i+1).split(":");
						start.set(0, 0, 0, Integer.valueOf(startTime[0]),Integer.valueOf(startTime[1]));
						end.set(0, 0, 0, Integer.valueOf(endTime[0]),Integer.valueOf(endTime[1]));
						info += (TrafficFlowActivity.format.format(start.getTime()) + " - " + TrafficFlowActivity.format.format(end.getTime()));
					}
					startTime = timings.get(timings.size()-2).split(":");
					endTime = timings.get(timings.size()-1).split(":");
					start.set(0, 0, 0, Integer.valueOf(startTime[0]),Integer.valueOf(startTime[1]));
					end.set(0, 0, 0, Integer.valueOf(endTime[0]),Integer.valueOf(endTime[1]));
					info += "<br/>"+(TrafficFlowActivity.format.format(start.getTime()) + " - " + TrafficFlowActivity.format.format(end.getTime()));
				} else {
					startTime = timings.get(0).split(":");
					endTime = timings.get(1).split(":");
					start.set(0, 0, 0, Integer.valueOf(startTime[0]),Integer.valueOf(startTime[1]));
					end.set(0, 0, 0, Integer.valueOf(endTime[0]),Integer.valueOf(endTime[1]));
					info += (TrafficFlowActivity.format.format(start.getTime()) + " - " + TrafficFlowActivity.format.format(end.getTime()));
				}
				info += "</font></small>";
			} else {
				info += "</font><font color='#4c4c4c'>Closed Today</font></small>";
			}
			facilityPic.setImageResource(Integer.valueOf((String)OpenGymActivity.facilityDetails.get(resources.get(position)).get(0)));
			facilityInfo.setText(Html.fromHtml(info));
			openClose.setTypeface(face);
			if(Integer.valueOf((String)OpenGymActivity.facilityDetails.get(resources.get(position)).get(3))==1){
				openClose.setText("Open");
				openClose.setTextColor(Color.rgb(1, 179, 1));
			} else {
				openClose.setText("Closed");
				openClose.setTextColor(Color.rgb(252, 12, 12));
			}
		}		
		return rowView;
	}

}
